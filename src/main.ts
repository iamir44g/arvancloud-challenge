import './assets/styles/index.css';

import { createApp } from 'vue';
import App from './App.vue';
import router from './router';


import PluginsInstaller from './lib/plugins';
const app = createApp(App);


app.use(router);
app.use(PluginsInstaller);

app.mount('#app');
