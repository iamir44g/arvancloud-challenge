

import { createRouter, createWebHistory, type RouteRecordRaw } from 'vue-router';
import { getFromStorage } from '../lib/services/storage';
const routes: Array<RouteRecordRaw> = [

	{
		path: '/auth',
		name: 'auth',
		redirect: '/auth/login',
		component: () => import('@/Layouts/Auth/AuthLayout.vue'),
		children: [
			{
				name: 'auth-login',
				path: 'login',

				component: () => import('@/Views/Auth/LoginView.vue'),
			},
			{
				name: 'auth-register',
				path: 'register',
				component: () => import('@/Views/Auth/RegisterView.vue'),
			},
		],
	},
	{
		name: 'dashboard',
		path: '/articles',
		meta: { isAuth: true },
		component: () => import('@/Layouts/Dashboard/DashboardLayout.vue'),
		children: [
			{
				name: 'dashboard-post-articles-list',
				path: 'list',
				component: () => import('@/Views/Dashboard/post/AllArticles/ArticlesList.vue'),
			},
			{
				name: 'dashboard-new-articles',
				path: 'create',
				component: () => import('@/Views/Dashboard/post/NewArticles/NewArticles.vue'),
			},
			{
				name: 'dashboard-edit-articles',
				path: 'edit/:slug',

				component: () => import('@/Views/Dashboard/post/EditArticle/EditArticle.vue'),
			},
		],
	}, {
		name: 'not-found',
		path: '/:pathMatch(.*)*',
		component: () => import('@/Layouts/Error/Error.vue'),
	},
];
const router = createRouter({
	history: createWebHistory(import.meta.env.BASE_URL),
	routes,
	linkExactActiveClass: "active",

});

function CheckLogin() {

	const token = getFromStorage('token');
	if(token === null || token === ''){
		return false;
	}
	return true;
}
router.beforeEach( function(to, from, next){

	if(to.name === 'auth-login' && CheckLogin()){
		next({ name: 'dashboard-post-articles-list' });
	}
	if(to.meta.isAuth && !CheckLogin()){
		next({ name: 'auth' });
	}else{
		next();
	}

});

export default router;
