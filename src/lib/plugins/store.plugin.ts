import type { Plugin } from 'vue';
import store from '../store/store';

const StorePlugin: Plugin = {
	install(app) {
		app.use(store);
	},
};

export default StorePlugin;
