/**
 * This file is not a real Vue plugin, instead its a plugin that registers other Vue plugins :)
 * Use the app instance to register any custom plugins inside the 'install' method.
 */
/**
 * This file is not a real Vue plugin, instead its a plugin that registers other Vue plugins :)
 * Use the app instance to register any custom plugins inside the 'install' method.
 */
import type { App } from 'vue';
import StorePlugin from './store.plugin';

export default {
	install(app: App): void {
		app.use(StorePlugin);
	},
};
