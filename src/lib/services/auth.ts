import type { AxiosRequestConfig } from 'axios';
import { callApi } from './api';

const ArvanChallengeApiUrl = import.meta.env.VITE_API_URL_V1;


export const postLogin = async (email:string, password:string) => {

	const config: AxiosRequestConfig = {
		url: `${ArvanChallengeApiUrl}/users/login`,
		method: 'POST',
		headers: {
			'content-type': 'application/json',
			'X-Requested-With': 'XMLHttpRequest',
		},
		data: {
			user: {
				email,
				password,
			},
		},
	};
	return callApi({ config });
};

export const postRegister = async (email:string, password:string, username:string) => {

	const config: AxiosRequestConfig = {
		url: `${ArvanChallengeApiUrl}/users`,
		method: 'POST',
		headers: {
			'content-type': 'application/json',
			'X-Requested-With': 'XMLHttpRequest',
		},
		data: {
			user: {
				email,
				password,
				username,
			},
		},
	};
	return callApi({ config });
};
