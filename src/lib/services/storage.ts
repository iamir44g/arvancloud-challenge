export const setInStorage = (key: string, value: any) => {
	try {
		localStorage.setItem(key, JSON.stringify(value));
		return 'OK';
	} catch (error) {
		return error;
	}
};

export const getFromStorage = (key: string) => {
	try {
		const result = localStorage.getItem(key);
		if (result) {
			return JSON.parse(result);
		}
		return result;
	} catch (error) {
		return error;
	}
};

export const removeFromStorage = (key: string) => {
	try {
		localStorage.removeItem(key);
		return 'OK';
	} catch (error) {
		return error;
	}
};

export const clearStorage = () => {
	try {
		localStorage.clear();
		return 'OK';
	} catch (error) {
		return error;
	}
};
