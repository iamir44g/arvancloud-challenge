import type { AxiosRequestConfig } from 'axios';
import { callApi } from './api';
import { getFromStorage } from './storage';
import type { Article } from '../types/newArticle/index';

const ArvanChallengeApiUrl = import.meta.env.VITE_API_URL_V1;


export const getArticles = async () => {

	const config: AxiosRequestConfig = {
		url: `${ArvanChallengeApiUrl}/articles`,
		method: 'GET',
		headers: {
			'content-type': 'application/json',
			'X-Requested-With': 'XMLHttpRequest',
		},

	};
	return callApi({ config });
};
export const getTags = async () => {

	const config: AxiosRequestConfig = {
		url: `${ArvanChallengeApiUrl}/tags`,
		method: 'GET',
		headers: {
			'content-type': 'application/json',
			'X-Requested-With': 'XMLHttpRequest',

		},

	};
	return callApi({ config });
};

export const deleteArticles = async (slug:string) => {

	const config: AxiosRequestConfig = {
		url: `${ArvanChallengeApiUrl}/articles/${slug}`,
		method: 'DELETE',
		headers: {
			'content-type': 'application/json',
			'X-Requested-With': 'XMLHttpRequest',
			'Authorization': `Token ${await getFromStorage('token')}`,
		},
	};
	return callApi({ config });
};

export const newArticle = async (newArticle:Article) => {

	const config: AxiosRequestConfig = {
		url: `${ArvanChallengeApiUrl}/articles`,
		method: 'POST',
		headers: {
			'content-type': 'application/json',
			'X-Requested-With': 'XMLHttpRequest',
			'Authorization': `Token ${await getFromStorage('token')}`,
		},
		data: {
			article: newArticle,

		},
	};
	return callApi({ config });
};

export const editArticle = async (editArticle:Article, slug:string) => {

	const config: AxiosRequestConfig = {
		url: `${ArvanChallengeApiUrl}/articles/${slug}`,
		method: 'PUT',
		headers: {
			'content-type': 'application/json',
			'X-Requested-With': 'XMLHttpRequest',
			'Authorization': `Token ${await getFromStorage('token')}`,
		},
		data: {
			article: editArticle,
		},
	};
	return callApi({ config });
};
