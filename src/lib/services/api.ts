import axios, { AxiosError, type AxiosResponse, type AxiosRequestConfig  } from 'axios';

import { type customError } from '@/lib/types/api/api-error';

export const callApi = async (options: { config: AxiosRequestConfig }): Promise<any> => {
	try {
		const response: AxiosResponse = await axios(options.config);
		const { data } = response;


		return {
			...data,
			error: null,
		};
	} catch (error) {
		if (axios.isAxiosError(error)) {
			const axiosError = error as AxiosError;

			const { response } = axiosError;

			let message = 'http request failed';

			if (response && response.statusText) {
				message = response.statusText;
			}

			if (axiosError.message) {

				message = axiosError.message;
			}

			if (response && response.data && (response.data as any).errors) {
				const errorMessage:customError = response.data.errors;
				message = `${Object.keys(errorMessage)[0] } ${Object.values(errorMessage)[0]}`;//(response.data as any).errors;
			}

			return {
				status: 'error',
				data: null,
				error: {
					message,
				},
			};
		}

		return {
			status: 'error',
			data: null,
			error: {
				message: (error as Error).message,
			},
		};
	}
};
