import { Store } from 'vuex';
import { State } from '@/lib/store/store';

declare module '@vue/runtime-core' {
	export interface ComponentCustomProperties {
		$store: Store<State>
	}
}

export {};
