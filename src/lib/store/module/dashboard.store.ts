
import type { Module } from 'vuex';

export interface DashboardState {
	sidebarCollapse: boolean
}

const store: Module<DashboardState, {}> = {
	namespaced: true,
	state () {
		return {
			sidebarCollapse: false,
		};
	},
	mutations: {
		toggleSidebarCollapse (state: DashboardState, status) {
			state.sidebarCollapse = status;

		},
	},
};

export default store;
