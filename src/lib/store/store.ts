import { createStore } from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import DashboardStore, { type DashboardState } from './module/dashboard.store';

export interface State {
	dashboard: DashboardState,
}

const store = createStore<State> ({
	modules: {
		dashboard: DashboardStore,
	},
	plugins: [
		createPersistedState({
			key: 'ArvanChallenge',
			paths: [
				'dashboard',
			],
		}),
	],
});

export default store;
