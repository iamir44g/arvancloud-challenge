export interface SidebarItem {
	title: string
	icon: string
	link: string
}

export interface SidebarCategory {
	category: string
	items: SidebarItem[]
}
