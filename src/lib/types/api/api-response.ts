import type { ApiError } from './api-error';

export interface ApiResponse {
	status: 'success' | 'fail' | 'error';
	data?: any | null;
	data_count?: number | null;
	message?: string | null;
	error_code?: number | null;
	error?: ApiError | null;
}
