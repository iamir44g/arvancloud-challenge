export interface MessageReport {
	title: string
	body: string
}
