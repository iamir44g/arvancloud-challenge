
export interface userLogin {
	email:string,
	password:string

}
export interface user {
	email:string,
	username:string,
	bio:string|any,
	image:string,
	token:string,
}
