export interface tag{
	name:string,
	isChecked:boolean
}
export interface articleItem {
	title:string,
	author:string,
	tags:string[],
	excerpt:string,
	created:string

}

export interface post {
	title: string
	description: string,
	body:string
}

export interface Author {
	username:  string;
	bio:       null;
	image:     string;
	following: boolean;
}

export interface Article {
	slug:           string;
	title:          string;
	description:    string;
	body:           string;
	tagList:        string[];
	createdAt:      Date;
	updatedAt:      Date;
	favorited:      boolean;
	favoritesCount: number;
	author:         Author;
}

export interface ArticleItems {
	articles:      Article[];
	articlesCount: number;
}




